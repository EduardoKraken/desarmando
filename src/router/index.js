import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

// USUARIOS
import Login           from '@/views/usuarios/Login.vue'
import Registro        from '@/views/usuarios/Registro.vue'
import RecuperarContra from '@/views/usuarios/RecuperarContra.vue'

import MenuLeccion     from '@/views/cursos/MenuLeccion.vue'

import Video         from '@/views/ejercicios/Video.vue'
import Hablar        from '@/views/ejercicios/Hablar.vue'
import Escribir      from '@/views/ejercicios/Escribir.vue'
import Interpreta    from '@/views/ejercicios/Interpreta.vue'
import Videoclase    from '@/views/ejercicios/Videoclase.vue'
import Vocabulario   from '@/views/ejercicios/Vocabulario.vue'
import Evaluacion    from '@/views/ejercicios/Evaluacion.vue'



Vue.use(VueRouter)

const routes = [
  { path: '/'               , name: 'home'            , component: HomeView        },
  { path: '/login'          , name: 'Login'           , component: Login           },
  { path: '/registro'       , name: 'Registro'        , component: Registro        },
  { path: '/recuperarcontra', name: 'RecuperarContra' , component: RecuperarContra },

  { path: '/menuleccion'    , name: 'MenuLeccion'     , component: MenuLeccion },

  { path: '/video'          , name: 'Video'           , component: Video },
  { path: '/hablar'         , name: 'Hablar'          , component: Hablar },
  { path: '/escribir'       , name: 'Escribir'        , component: Escribir },
  { path: '/interpreta'     , name: 'Interpreta'      , component: Interpreta },
  { path: '/videoclase'     , name: 'Videoclase'      , component: Videoclase },
  { path: '/vocabulario'    , name: 'Vocabulario'     , component: Vocabulario },
  { path: '/evaluacion'     , name: 'Evaluacion'      , component: Evaluacion },

]

const router = new VueRouter({
  routes
})

export default router
